# FIBerHub

![Version](https://img.shields.io/badge/version-1.2-blue.svg)![Version](https://img.shields.io/badge/release%20date-february%202019-green.svg) ![Version](https://img.shields.io/badge/source%20code%20license-MIT-green.svg) *![Version](https://img.shields.io/badge/contributors-5-green.svg)*

## What's this?

FIBerHub is a website repository where you'll find all kinds of stuff that will help you when studying Computer Engineering at UPC. The content ranges from official subject notes, exams or program codes to solutions for scoring questionnaires and other useful tools (SISA Converter, etc.)

## About us

We're a small group of students who want to help others by providing solutions to make their study life easier.

If you have any doubts or you need any help don't hesitate to contact us! You can find us on Instagram:  **_fiberhub**

## Want to help?

Want to file a bug, contribute on some code, or improve documentation? Excellent! Just contact us and we'll answer you as soon as possible.