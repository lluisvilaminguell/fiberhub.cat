# FIBerHub

![Version](https://img.shields.io/badge/version-1.2-blue.svg)![Version](https://img.shields.io/badge/release%20date-february%202019-green.svg) ![Version](https://img.shields.io/badge/source%20code%20license-MIT-green.svg) *![Version](https://img.shields.io/badge/contributors-5-green.svg)*

## ¿Qué es esto?

FIBerHub es un repositorio donde encontraréis todo tipo de material que os ayudará si estudiáis Ingeniería Informática en la UPC. Encontraréis desde apuntes oficiales de la asignatura, exámenes o códigos de programas hasta soluciones de cuestionarios puntuables y otras herramientas útiles (Conversor SISA, etc.)

## ¿Quienes somos?

Somos un pequeño grupo de estudiantes que quiere ayudar a los demás ofreciendo soluciones para facilitar la vida estudiantil.

Si tenéis cualquier duda o necesitáis ayuda no dudéis en contactar con nosotros!  Podéis encontrarnos en Instagram: **_fiberhub**

## ¿Quieres ayudarnos?

¿Quieres reportar un bug, contribuir con un código o mejorar la documentación? ¡Excelente! Contacta con nosotros y te responderemos lo más rápido posible.